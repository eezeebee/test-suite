# Laravel test suite

## Installation

Copy the following code in the composer.json of your project under scripts section

```
"post-install-cmd": [
    "@php -r \"copy('vendor/eezeebee/test-suite/config/bitbucket-pipelines.yml', 'bitbucket-pipelines.yml');\"",
    "@php -r \"copy('vendor/eezeebee/test-suite/config/grumphp.yml', 'grumphp.yml');\""
],
"post-update-cmd": [
    "@php -r \"copy('vendor/eezeebee/test-suite/config/bitbucket-pipelines.yml', 'bitbucket-pipelines.yml');\"",
    "@php -r \"copy('vendor/eezeebee/test-suite/config/grumphp.yml', 'grumphp.yml');\""
 ]
```

Install package with composer

`composer require eezeebee/test-suite`

## Usage

#### Automatic

The test suite (grumphp) is run automaticaly on creating a pull request. The test suite is run before you can commit. In bitbucket the test suite is run when you create an pull request.

#### Manual

Run the following command to run the test suite manually

```
php ./vendor/bin/grumphp run
```

## Common problems

#### False postives phpmd

You can supress false positives for phpmd with an annotation. Due to dependency injection and class hierarchy it's possible 
that you get an false positive for unused variables. But you can't remove them because laravel expects them.
To supress these warnings use an annotation in the class comment

* @SuppressWarnings("unused")

#### Memory error phpstan

When running the grump tasks it's possible that you get an memory error. This occurs when there are a lot of errors (most common on first run). To see te results run phpstan manually with the following command:

```
php artisan code:analyse
```